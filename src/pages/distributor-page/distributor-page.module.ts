import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DistributorPage} from './distributor-page';

@NgModule({
  declarations: [
    DistributorPage,
  ],
  imports: [
    IonicPageModule.forChild(DistributorPage),
  ],
  exports: [
    DistributorPage
  ]
})
export class DistributorPageModule {}