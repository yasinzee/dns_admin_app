import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'distributor-page',
    templateUrl: 'distributor-page.html'
})
export class DistributorPage {
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,) {

    }
    
    openPage(page: string, forwardTo:any) {
        console.log("clicked" + page);
        this.navCtrl.push(page, {forwardToPage: forwardTo});
    }

}
