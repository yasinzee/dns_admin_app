import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { ExtraComponents } from '../../providers/extra-components';

@IonicPage()
@Component({
  selector: 'home-page',
  templateUrl: 'home-page.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
    public extraComponents: ExtraComponents,
    public menuCtrl: MenuController, ) {
    menuCtrl.enable(true);
    this.navCtrl.swipeBackEnabled = false;
  }
  openPage(page: string) {
    console.log("clicked" + page);
    if (page == 'CONSUMER')
      this.extraComponents.showAlert("Message", new String("Work Under Progress!!!"));
    else
      this.navCtrl.push(page);
  }
}
