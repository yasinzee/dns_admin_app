import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';

@IonicPage()
@Component({
    selector: 'register-distributor',
    templateUrl: 'register-distributor-page.html',
})

export class RegisterDistributorPage {
    distributorData: FormGroup;

    constructor(
        public navCtrl: NavController,
        public menuCtrl: MenuController,
        public formBuilder: FormBuilder,
        public extraComponents: ExtraComponents,
        public http: Http) {

        this.distributorData = this.formBuilder.group({
            firstName: ['', Validators.compose([Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            lastName: ['',Validators.compose([Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            email: ['', Validators.compose([Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])],
            password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
            mobile: ['', Validators.compose([Validators.required, Validators.pattern("[0-9]{10}")])],
            gender: ['', Validators.required],
            dateOfBirth: ['', Validators.required],
            state: ['', Validators.required],
            city: ['', Validators.required],
            pincode: ['', Validators.required],
            address: ['', Validators.required],
        });
    }


    ionViewDidLoad() {
    }


    register(formData: any) {
        this.extraComponents.showLoading();
        this.storeDistributor(formData).toPromise()
            .then(result => {
                this.extraComponents.closeLoading();
                if(result.status==208){
                    console.log("Dstributor already exist");
                    console.log(result);
                    this.extraComponents.showAlert("Error", new String("email already exist"));
                }
                else if(result.status==400){
                    console.log("Error");
                    console.log(result);
                    this.extraComponents.showAlert("Error", new String("Internal Server Error. Plz Try Again!!"));
                }
                else{
                    console.log("Stored Successfully");
                    console.log(result);
                    this.extraComponents.showAlert("Success", new String("Dstributor Added Successfully"));
                    this.navCtrl.setRoot('HomePage');
                }
            }).catch(error => {
                this.extraComponents.closeLoading();
                this.extraComponents.showAlert("Error", error);
            });
    }

    storeDistributor(formData: any) {
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });

        formData.name = formData.firstName + " " + formData.lastName;
        console.log("Form Data");
        console.log(formData);
        console.log("url: " + AppConfig.cfg.apiUrl + AppConfig.cfg.addDistributor);
        return this.http.post(AppConfig.cfg.apiUrl + AppConfig.cfg.addDistributor, formData, options);
    }
}

