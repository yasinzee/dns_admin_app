import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, ModalController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import *  as AppConfig from '../../app/config';
import {SearchFilterModal} from '../filter-modal/search-filter-modal';

@IonicPage()
@Component({
    selector: 'search-page-for-distributor',
    templateUrl: 'search-page-for-distributor.html'
})
export class SearchPageForDistributor {
    private productList: any = [];
    private cities: any = [];
    private distributor:any;
    private consumerList:any=[];
    private searchDistributorList: any = [];
    private totalPrice: any = 0;
    private filledFilterFields: any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public modalCtrl: ModalController,
        public formBuilder: FormBuilder,
        public http: Http) {

    }

    ionViewDidLoad() {
        console.log("called");
        this.productList = this.navParams.get('productList');
        this.cities = this.navParams.get('citiesList');
        
    }

    openModal() {
        let modal = this.modalCtrl.create(SearchFilterModal, { productList: this.productList, citiesList: this.cities, filledFilterFields: this.filledFilterFields, caller:AppConfig.finalVar.distributor });
        modal.present();
        modal.onDidDismiss(data => {
            console.log(data);
            if (data != null && data != undefined) {
                this.searchDistributorList = data.result;
                this.filledFilterFields = data.filledFilterFields;
            }

            if (this.searchDistributorList != null || this.searchDistributorList != undefined) {
                this.totalPrice=0;
                this.searchDistributorList.forEach(element => {
                    this.totalPrice += element.totalAmount;
                });
            }
            else {
                this.totalPrice = 0;
            }

        });

    }

    viewDetail(distributor:any){
        this.navCtrl.push('DistributorFilterDetailPage', {id:distributor.distributor.id, filter:this.filledFilterFields, subtotal: distributor.totalAmount});
    }
}
