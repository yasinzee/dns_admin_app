import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SearchPageForDistributor} from './search-page-for-distributor';

@NgModule({
  declarations: [
    SearchPageForDistributor,
  ],
  imports: [
    IonicPageModule.forChild(SearchPageForDistributor),
  ],
  exports: [
    SearchPageForDistributor
  ]
})
export class SearchPageForDistributorModule {}