import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';

@IonicPage()
@Component({
    selector: 'search-page',
    templateUrl: 'search-page.html'
})
export class SearchPage {
    private productList: any = [];
    private cities: any = [];
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public http: Http) {
    }

    ionViewDidLoad() {
        this.getAllProducts().toPromise()
            .then(result => {
                this.productList = result.json();
                console.log(this.productList);

            }).catch(error => {
                console.log("Error while fetching Product list" + error);
            });

        this.getAllCities().toPromise()
            .then(result => {
                this.cities = result.json();
                console.log(this.cities);

            }).catch(error => {
                console.log("Error while fetching Product list" + error);
            });
    }

    getAllProducts() {
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllProducts);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllProducts, options);
    }
    getAllCities() {
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllCity);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllCity, options);
    }
    
    openPage(page: string) {
        this.navCtrl.push(page, {productList:this.productList, citiesList:this.cities});
    }
}
