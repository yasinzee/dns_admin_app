import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { ExtraComponents } from '../../providers/extra-components';


@IonicPage()
@Component({
    selector: 'inventory-page',
    templateUrl: 'inventory-page.html'
})
export class InventoryPage {
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents,) {

    }
    
    openPage(page: string, forwardTo:any) {
        console.log("clicked" + page);
        this.navCtrl.push(page, {forwardToPage: forwardTo});
    }
}
