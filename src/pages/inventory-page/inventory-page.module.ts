import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {InventoryPage} from './inventory-page';

@NgModule({
  declarations: [
    InventoryPage,
  ],
  imports: [
    IonicPageModule.forChild(InventoryPage),
  ],
  exports: [
    InventoryPage
  ]
})
export class InventoryPageModule {}