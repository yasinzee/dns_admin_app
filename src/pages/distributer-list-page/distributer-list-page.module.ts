
import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DistributerListPage} from './distributer-list-page';

@NgModule({
  declarations: [
    DistributerListPage,
  ],
  imports: [
    IonicPageModule.forChild(DistributerListPage),
  ],
  exports: [
    DistributerListPage
  ]
})
export class DistributerListPageModule {}