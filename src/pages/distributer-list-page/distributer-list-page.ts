import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { ExtraComponents } from '../../providers/extra-components';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';

@IonicPage()
@Component({
    selector: 'distributer-list-page',
    templateUrl: 'distributer-list-page.html'
})
export class DistributerListPage {
    private distributorList: any = [];
    private distributor:any = [];

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents, 
        public http: Http) {

    }
    ionViewWillEnter() {
        this.extraComponents.showLoading();
        this.getDistributorList().toPromise()
            .then(result => {
                this.extraComponents.closeLoading();
                console.log(result.json());
                this.distributorList = result.json();
            }).catch(error => {
                this.extraComponents.closeLoading();
                this.extraComponents.showAlert("error", error);
                console.log("Error while fetching distributor list" + error);
            });
    }

    getDistributorList() {
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllDistributor);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllDistributor, options);
    }


    proceed(){
        console.log("proceed " + this.distributor);
        if(this.navParams.get('forwardToPage') == 'UPDATE_INVENTORY')
            this.navCtrl.push('UpdateInventoryPage', {distributorId:this.distributor});
        else{
            let distributorDetail = this.distributorList.find(d=> d.id==this.distributor);
            this.navCtrl.push('ChangeDistributorPasswordPage', {distributorId:distributorDetail});
        }
            
    }

}
