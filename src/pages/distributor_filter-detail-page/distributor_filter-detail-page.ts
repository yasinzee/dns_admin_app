import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';


@IonicPage()
@Component({
    selector: 'distributor_filter-detail-page',
    templateUrl: 'distributor_filter-detail-page.html'
})
export class DistributorFilterDetailPage {
    private distributorDetails:any=[];
    private subtotal:any;
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents,
        public http: Http) {
            console.log(this.navParams.get('filter'));
            this.subtotal = this.navParams.get('subtotal');
    }
    ionViewDidLoad() {
        this.getDetails().toPromise()
            .then(result => {
                this.distributorDetails = result.json();
                console.log(this.distributorDetails);

            }).catch(error => {
                console.log("Error while fetching Product list" + error);
            });   
    }
    getDetails(){
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        let param = "?id="+ this.navParams.get('id') + "&fromDate=" + this.navParams.get('filter').fromDate + "&toDate=" + this.navParams.get('filter').toDate;
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.distributorFilterResultDetail + param);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.distributorFilterResultDetail
             + param, options);
    }
}
