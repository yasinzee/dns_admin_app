import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {DistributorFilterDetailPage} from './distributor_filter-detail-page';

@NgModule({
  declarations: [
    DistributorFilterDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(DistributorFilterDetailPage),
  ],
  exports: [
    DistributorFilterDetailPage
  ]
})
export class DistributorFilterDetailPageModule {}