import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';

@IonicPage()
@Component({
    selector: 'single-product-view-page',
    templateUrl: 'single-product-view-page.html'
})
export class SingleProductViewPage {
    private productData: FormGroup;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public formBuilder: FormBuilder,
        public extraComponents: ExtraComponents,
        public http: Http,
        private imagePicker: ImagePicker,
        private base64: Base64) {

            console.log(this.navParams.get('productDetail'));
            this.productData = this.formBuilder.group({
                name: [null, Validators.required],
                price: [null,Validators.required],
                
            });
            if(this.navParams.get('productDetail'))
                this.initializeFields(this.navParams.get('productDetail'));
    }
    
    change(){
        
    }

    initializeFields(product:any){
        this.productData.controls['name'].setValue(product.name);
        this.productData.controls['price'].setValue(product.price);
    }

}

