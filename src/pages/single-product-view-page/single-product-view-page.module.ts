import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SingleProductViewPage} from './single-product-view-page';

@NgModule({
  declarations: [
    SingleProductViewPage,
  ],
  imports: [
    IonicPageModule.forChild(SingleProductViewPage),
  ],
  exports: [
    SingleProductViewPage
  ]
})
export class SingleProductViewPageModule {}