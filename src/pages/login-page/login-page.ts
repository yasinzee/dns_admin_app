import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';

@IonicPage()
@Component({
    selector: 'login-page',
    templateUrl: 'login-page.html',
})

export class LoginPage {
    private loginData: FormGroup;
    private invalidLogin: boolean;
    constructor(
        public navCtrl: NavController,
        public menuCtrl: MenuController,
        public formBuilder: FormBuilder,
        public http: Http,
        public extraComponents: ExtraComponents) {
        this.loginData = this.formBuilder.group({
            email: ['', Validators.compose([Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])],
            password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        });
        this.invalidLogin = false;
    }


    ionViewDidLoad() {
        //hide menu when on the login page, regardless of the screen resolution
        // this.menuCtrl.enable(false);
    }

    ionViewCanEnter() {
        //hide menu when on the login page, regardless of the screen resolution

        if (localStorage.getItem('distributor')) {
            setTimeout(() => {
                this.navCtrl.setRoot('HomePage');
            });
            console.log("Already Logged In");
            
            return false;
        }
        else {
            this.menuCtrl.enable(false);
            return true;
        }
    }

    login() {
        this.extraComponents.showLoading();
        this.authenticate().toPromise()
            .then(result => {
                this.extraComponents.closeLoading();
                console.log("Logged in Successfully");
                console.log(result.json());
                localStorage.setItem("distributor", JSON.stringify(result.json()));
                this.navCtrl.setRoot('HomePage');
            }).catch(error => {
                this.extraComponents.closeLoading();
                console.log("Invalid username/password" + error);
                // this.extraComponents.showAlert("error", error);
                this.invalidLogin = true;
            });
    }

    authenticate() {
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        let url = AppConfig.cfg.apiUrl + AppConfig.cfg.authenticate;
        let param = "?username=" + this.loginData.value.email + "&password=" + this.loginData.value.password;
        console.log("Form Data");
        console.log(this.loginData.value);
        console.log("url: " + AppConfig.cfg.apiUrl + AppConfig.cfg.authenticate);
        return this.http.post(url + param, "", options);
    }

    openPage(page: string) {
        this.navCtrl.push(page);
    }
}

