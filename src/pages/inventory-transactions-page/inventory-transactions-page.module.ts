import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {InventoryTransactionPage} from './inventory-transactions-page';

@NgModule({
  declarations: [
    InventoryTransactionPage,
  ],
  imports: [
    IonicPageModule.forChild(InventoryTransactionPage),
  ],
  exports: [
    InventoryTransactionPage
  ]
})
export class InventoryTransactionPageModule {}