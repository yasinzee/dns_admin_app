import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ExtraComponents } from '../../providers/extra-components';
import *  as AppConfig from '../../app/config';


@IonicPage()
@Component({
    selector: 'inventory-transactions-page',
    templateUrl: 'inventory-transactions-page.html'
})
export class InventoryTransactionPage {
    private inventoryTransactions:any=[];
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents,
        public http: Http,) {

    }
    ionViewDidLoad() {
        this.getAllInventoryTransactions().toPromise()
            .then(result => {
                this.inventoryTransactions = result.json();
                console.log(this.inventoryTransactions);

            }).catch(error => {
                console.log("Error while fetching Product list" + error);
            });   
    }

    getAllInventoryTransactions(){
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        let param = "?id="+ JSON.parse(localStorage.getItem('distributor')).id;
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllInventoryTransaction);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllInventoryTransaction + param, options);
    }

}
