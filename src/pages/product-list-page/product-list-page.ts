import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { ExtraComponents } from '../../providers/extra-components';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';

@IonicPage()
@Component({
    selector: 'product-list-page',
    templateUrl: 'product-list-page.html'
})
export class ProductListPage {
    private productList: any = [];

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents, 
        public http: Http) {

    }
    ionViewWillEnter() {
        this.extraComponents.showLoading();
        this.getProductList().toPromise()
            .then(result => {
                this.extraComponents.closeLoading();
                console.log(result.json());
                this.productList = result.json();
            }).catch(error => {
                this.extraComponents.closeLoading();
                this.extraComponents.showAlert("error", error);
                console.log("Error while fetching Product list" + error);
            });
    }

    getProductList() {
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllProducts);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllProducts, options);
    }


    proceed(product:any){
        this.navCtrl.push('SingleProductViewPage', {productDetail:product});
    }

}
