
import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ProductListPage} from './product-list-page';

@NgModule({
  declarations: [
    ProductListPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductListPage),
  ],
  exports: [
    ProductListPage
  ]
})
export class ProductListPageModule {}