import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { IonicPage, ModalController, Platform, NavParams, ViewController } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';

@Component({
    templateUrl: 'search-filter-modal.html'
})
export class SearchFilterModal {
    private filterGroup: FormGroup;
    private productList: any = [];
    private cities: any = [];
    private distributor: any;
    private consumerList: any = [];
    private errorMsg:any;

    constructor(public modalCtrl: ModalController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public viewCtrl: ViewController,
        public http: Http) {

        this.filterGroup = this.formBuilder.group({
            fromDate: [null, Validators.required],
            toDate: [null, Validators.required],
            city: [null, Validators.required],
            distributor: [null, Validators.required],
            consumer: [null, Validators.required],
            product: [null, Validators.required]
        });
        this.productList = this.navParams.get('productList');
        this.cities = this.navParams.get('citiesList');

        this.autoFillFilter(this.navParams.get('filledFilterFields'));
        console.log(this.navParams.get('filledFilterFields'));
        console.log(this.navParams.get('caller'));

    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    cityChange(event: any) {
        console.log("changed");
        this.getCityWiseDistributorAndConsumer(event).toPromise()
            .then(result => {
                console.log(result.json());
                this.distributor = result.json().distributor;
                this.consumerList = result.json().consumerList;
                this.filterGroup.controls['distributor'].setValue(this.distributor.name);
            }).catch(error => {
                console.log("Error while fetching Product list" + error);
            });
        console.log(event);
    }


    getCityWiseDistributorAndConsumer(city: any) {
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        let queryParam = "?city=" + city;
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchCityWiseConsumerAndDistributor + queryParam);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchCityWiseConsumerAndDistributor + queryParam, options);
    }

    submit() {
        if (this.filterGroup.value.fromDate != null && this.filterGroup.value.fromDate != null) {

            let headers = new Headers();
            headers.append("Accept", "application/json");
            headers.append('Content-Type', 'application/json');
            let options = new RequestOptions({ headers: headers });

            console.log("Form Data");
            console.log(this.filterGroup.value);
            if (this.distributor != null || this.distributor != undefined)
                this.filterGroup.value.distributor = this.distributor.id;

            let url = AppConfig.cfg.apiUrl;
            if (this.navParams.get('caller') == AppConfig.finalVar.product)
                url += AppConfig.cfg.productWiseSearch;
            else if (this.navParams.get('caller') == AppConfig.finalVar.distributor)
                url += AppConfig.cfg.distributorWiseSearch;
            else if (this.navParams.get('caller') == AppConfig.finalVar.consumer)
                url += AppConfig.cfg.consumerWiseSearch;

            console.log(url);
            this.http.post(url, this.filterGroup.value, options).toPromise()
                .then(result => {
                    console.log(result.json());
                    this.viewCtrl.dismiss({ result: result.json(), filledFilterFields: this.filterGroup.value });
                }).catch(error => {
                    console.log("Error while fetching filtered Product data" + error);
                });
        }
        else
            this.errorMsg="Required Fields";
    }

    autoFillFilter(filledData: any) {
        if (filledData != null && filledData != undefined) {
            if (filledData.fromDate != null && filledData.fromDate != undefined) {
                this.filterGroup.controls['fromDate'].setValue(filledData.fromDate);
            }
            if (filledData.toDate != null && filledData.toDate != undefined) {
                this.filterGroup.controls['toDate'].setValue(filledData.toDate);
            }
        }
        
    }
}
