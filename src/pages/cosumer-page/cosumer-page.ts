import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { ExtraComponents } from '../../providers/extra-components';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import *  as AppConfig from '../../app/config';


@IonicPage()
@Component({
    selector: 'cosumer-page',
    templateUrl: 'cosumer-page.html'
})
export class ConsumerPage {
    private consumerForm: FormGroup;
    private consumers:any=[];
    private cities:any=[];
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents,
        public formBuilder: FormBuilder,
        public http: Http) {

        this.consumerForm = this.formBuilder.group({
            city: [null, Validators.required],
        });
    }

    ionViewDidLoad() {
        this.getAllCities().toPromise()
            .then(result => {
                this.cities = result.json();
                console.log(this.cities);

            }).catch(error => {
                console.log("Error while fetching Product list" + error);
            });  
    }

    getAllCities() {
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllCity);

        return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchAllCity, options);
    }

    submit() {
        let headers = new Headers();
        headers.append("Accept", "application/json");
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });
        let param = "?city="+this.consumerForm.value.city;
        console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchConsumerListByCity + param);

        this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.fetchConsumerListByCity+ param, options).toPromise()
        .then(result => {
            this.consumers = result.json();
            console.log(this.consumers);

        }).catch(error => {
            console.log("Error while fetching Consumer list" + error);
        });  
    }

    viewDetail(consumer:any){
        this.navCtrl.push('ConsumerDetailPage', {consumerDetail:consumer});
        
    }
}

