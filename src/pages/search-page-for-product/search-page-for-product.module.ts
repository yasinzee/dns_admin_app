import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SearchPageForProduct} from './search-page-for-product';

@NgModule({
  declarations: [
    SearchPageForProduct,
  ],
  imports: [
    IonicPageModule.forChild(SearchPageForProduct),
  ],
  exports: [
    SearchPageForProduct
  ]
})
export class SearchPageForProductModule {}