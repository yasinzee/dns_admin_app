import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, ModalController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import *  as AppConfig from '../../app/config';
import { SearchFilterModal } from '../filter-modal/search-filter-modal';

@IonicPage()
@Component({
    selector: 'search-page-for-product',
    templateUrl: 'search-page-for-product.html'
})
export class SearchPageForProduct {
    filterGroup: FormGroup;
    private productList: any = [];
    private cities: any = [];
    private distributor: any;
    private consumerList: any = [];
    private searchProductList: any = [];
    private totalPrice: any = 0;
    private filledFilterFields: any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public modalCtrl: ModalController,
        public formBuilder: FormBuilder,
        public http: Http) {

        this.filterGroup = this.formBuilder.group({
            fromDate: [null, Validators.required],
            toDate: [null, Validators.required],
            city: [null, Validators.required],
            distributor: [null, Validators.required],
            consumer: [null, Validators.required],
            product: [null, Validators.required]
        });

        this.productList = this.navParams.get('productList');
        this.cities = this.navParams.get('citiesList');
    }

    ionViewDidLoad() {
    }

    openModal() {
        let modal = this.modalCtrl.create(SearchFilterModal, { productList: this.productList, citiesList: this.cities, filledFilterFields: this.filledFilterFields, caller:AppConfig.finalVar.product });
        modal.present();
        modal.onDidDismiss(data => {
            console.log(data);
            if (data != null && data != undefined) {
                this.searchProductList = data.result;
                this.filledFilterFields = data.filledFilterFields;
            }

            if (this.searchProductList != null || this.searchProductList != undefined) {
                this.totalPrice=0;
                this.searchProductList.forEach(element => {
                    this.totalPrice += element.totalAmount;
                });
            }
            else {
                this.totalPrice = 0;
            }

        });
    }

    
}
