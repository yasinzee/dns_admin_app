import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { ExtraComponents } from '../../providers/extra-components';


@IonicPage()
@Component({
    selector: 'product-page',
    templateUrl: 'product-page.html'
})
export class ProductPage {
    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents,) {

    }
    
    openPage(page: string) {
        console.log("clicked" + page);
        if(page==='ProductListPage')
            this.navCtrl.push(page);
        else
            this.extraComponents.showAlert("Message", new String("Work Under Progress!!!"));   
    }

}
