import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ChangeDistributorPasswordPage} from './change-distributor-password';

@NgModule({
  declarations: [
    ChangeDistributorPasswordPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangeDistributorPasswordPage),
  ],
  exports: [
    ChangeDistributorPasswordPage
  ]
})
export class ChangeDistributorPasswordPageModule {}