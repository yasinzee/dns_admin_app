import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Http, Headers, RequestOptions } from '@angular/http';
import *  as AppConfig from '../../app/config';
import { ExtraComponents } from '../../providers/extra-components';

@IonicPage()
@Component({
    selector: 'change-distributor-password',
    templateUrl: 'change-distributor-password.html'
})
export class ChangeDistributorPasswordPage {
    private newPassword: any;
    private distributorData: FormGroup;
    private isReadOnly:any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents,
        public http: Http) {

        this.distributorData = this.formBuilder.group({
            name: [null, Validators.compose([Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            email: [null, Validators.compose([Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])],
            password: [null, Validators.compose([Validators.minLength(6), Validators.required])],
            mobile: [null, Validators.compose([Validators.required, Validators.pattern("[0-9]{10}")])],
            state: [null, Validators.required],
            city: [null, Validators.required],
            address: [null, Validators.required],
        });

        this.populateFields(this.navParams.get('distributorId'));
        this.isReadOnly = true;
    }

    update(distributorData:any) {
        if(!this.isReadOnly){
            distributorData.id=this.navParams.get('distributorId').id;
            // let param: any = '?id=' + this.navParams.get('distributorId') + '&current=null&new=' + this.newPassword;
            let headers = new Headers();
            headers.append("Accept", "application/json");
            headers.append('Content-Type', 'application/json');
            let options = new RequestOptions({ headers: headers });
            console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.updateDistributor);
            this.http.post(AppConfig.cfg.apiUrl + AppConfig.cfg.updateDistributor, distributorData, options).toPromise()
                .then(result => {
                    console.log(result.json());
                    this.extraComponents.showAlert("Success", new String("Updated successfully"));
                    this.navCtrl.setRoot('HomePage');
                }).catch(error => {
                    console.log(error);
                });
        }
        
    }

    populateFields(distributor:any){
        this.distributorData.controls['name'].setValue(distributor.name);
        this.distributorData.controls['email'].setValue(distributor.email);
        this.distributorData.controls['password'].setValue(distributor.password);
        this.distributorData.controls['mobile'].setValue(distributor.mobile);
        this.distributorData.controls['state'].setValue(distributor.state);
        this.distributorData.controls['city'].setValue(distributor.city);
        this.distributorData.controls['address'].setValue(distributor.address);
    }

    edit(){
        this.isReadOnly=!this.isReadOnly;
    }
}

