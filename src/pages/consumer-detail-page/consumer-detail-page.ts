import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { ExtraComponents } from '../../providers/extra-components';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import *  as AppConfig from '../../app/config';


@IonicPage()
@Component({
    selector: 'consumer-detail-page',
    templateUrl: 'consumer-detail-page.html'
})
export class ConsumerDetailPage {
    private consumerData: FormGroup;
    private consumer: any;
    private isReadOnly:any;

    constructor(public navCtrl: NavController,
        public navParams: NavParams,
        public menuCtrl: MenuController,
        public extraComponents: ExtraComponents,
        public formBuilder: FormBuilder,
        public http: Http) {
        this.consumerData = this.formBuilder.group({
            name: [null, Validators.compose([Validators.pattern('[a-zA-Z ]*'), Validators.required])],
            email: [null, Validators.compose([Validators.required, Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)])],
            mobile: [null, Validators.compose([Validators.required, Validators.pattern("[0-9]{10}")])],
            state: [null, Validators.required],
            city: [null, Validators.required],
            address: [null, Validators.required],
        });
        this.populateFields(this.navParams.get('consumerDetail'));
        this.isReadOnly = true;
    }

    update(consumerData:any) {
        if(!this.isReadOnly){
            consumerData.id=this.navParams.get('consumerDetail').id;
            // let param: any = '?id=' + this.navParams.get('distributorId') + '&current=null&new=' + this.newPassword;
            let headers = new Headers();
            headers.append("Accept", "application/json");
            headers.append('Content-Type', 'application/json');
            let options = new RequestOptions({ headers: headers });
            console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.updateConsumer);
            this.http.post(AppConfig.cfg.apiUrl + AppConfig.cfg.updateConsumer, consumerData, options).toPromise()
                .then(result => {
                    console.log(result.json());
                    this.extraComponents.showAlert("Success", new String("Updated successfully"));
                    this.navCtrl.setRoot('HomePage');
                }).catch(error => { 
                    console.log(error);
                });
        }
        
    }

    populateFields(consumer:any){
        this.consumerData.controls['name'].setValue(consumer.name);
        this.consumerData.controls['email'].setValue(consumer.email);
        this.consumerData.controls['mobile'].setValue(consumer.mobile);
        this.consumerData.controls['state'].setValue(consumer.state);
        this.consumerData.controls['city'].setValue(consumer.city);
        this.consumerData.controls['address'].setValue(consumer.address);
    }
    edit(){
        this.isReadOnly=!this.isReadOnly;
    }
}

