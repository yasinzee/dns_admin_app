import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {ConsumerDetailPage} from './consumer-detail-page';

@NgModule({
  declarations: [
    ConsumerDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(ConsumerDetailPage),
  ],
  exports: [
    ConsumerDetailPage
  ]
})
export class ConsumerDetailPageModule {}