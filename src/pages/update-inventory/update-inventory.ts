import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ExtraComponents } from '../../providers/extra-components';
import *  as AppConfig from '../../app/config';
import { AlertController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'update-inventory',
  templateUrl: 'update-inventory.html'
})
export class UpdateInventoryPage {
  private productList: any = [];
  private inventory: any = [];
  private distributorId: any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public extraComponents: ExtraComponents,
    public http: Http,
    public alerCtrl: AlertController) {

    this.distributorId = this.navParams.get('distributorId');
  }

  ionViewDidLoad() {
    this.extraComponents.showLoading();
    this.getInventoryDetails().toPromise()
      .then(result => {
        this.extraComponents.closeLoading();
        console.log(result.json());
        this.productList = result.json();
      }).catch(error => {
        this.extraComponents.closeLoading();
        console.log("Error while fetching inventory list" + error);
      });
  }

  getInventoryDetails() {
    let param: any = '?id=' + this.distributorId;
    let headers = new Headers();
    headers.append("Accept", "application/json");
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.inventoryDetails + param);

    return this.http.get(AppConfig.cfg.apiUrl + AppConfig.cfg.inventoryDetails + param, options);
  }

  updateIventory(inventoryId: any, stock: any, action: any) {
    let param: any = '?id=' + inventoryId + '&stock=' + stock + '&action=' + action + '&admin=' + JSON.parse(localStorage.getItem('distributor')).id;
    let headers = new Headers();
    headers.append("Accept", "application/json");
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });
    console.log(AppConfig.cfg.apiUrl + AppConfig.cfg.updateInventory + param);

    return this.http.put(AppConfig.cfg.apiUrl + AppConfig.cfg.updateInventory + param, options);
  }

  submit(product: any, action: any) {
    console.log(action);

    console.log(product);
    console.log(this.inventory[product.productId.id]);
    if (this.inventory[product.productId.id] == undefined || this.inventory[product.productId.id] == 0 || this.inventory[product.productId.id] == "") {
      this.extraComponents.showAlert("Warning", new String("Value must be greater than 0"));
    }
    else if(action === AppConfig.finalVar.inventorySubtract && (this.inventory[product.productId.id]>(product.totalStock-product.soldStock)) ){
      this.extraComponents.showAlert("Warning", new String("Value must be less than available stock while subtracting"))
    }
    else{
      let confirm = this.alerCtrl.create({
        title: 'Update Inventory',
        message: " Are you sure to Update the Inventory?",
        buttons: [
          {
            text: 'Disagree',
            handler: () => {
              console.log('Disagree clicked');
            }
          },
          {
            text: 'Agree',
            handler: () => {

              this.updateIventory(product.id, this.inventory[product.productId.id], action).toPromise()
                .then(result => {
                  console.log(result.json());
                  this.ionViewDidLoad();
                }).catch(error => {
                  console.log("Error while updating inventory list" + error);
                });
            }
          }
        ]
      });
      confirm.present()
    }
  }


}
