import {NgModule} from '@angular/core';
import {IonicPageModule} from 'ionic-angular';
import {SearchPageForConsumer} from './search-page-for-consumer';

@NgModule({
  declarations: [
    SearchPageForConsumer,
  ],
  imports: [
    IonicPageModule.forChild(SearchPageForConsumer),
  ],
  exports: [
    SearchPageForConsumer
  ]
})
export class SearchPageForConsumerModule {}