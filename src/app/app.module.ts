import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { ImagePicker } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { ExtraComponents } from '../providers/extra-components';

import { MyApp } from './app.component';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HttpModule } from '@angular/http';
import {SearchFilterModal} from '../pages/filter-modal/search-filter-modal';

@NgModule({
  declarations: [
    MyApp, SearchFilterModal
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp, SearchFilterModal
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ExtraComponents,
    ImagePicker,
    Base64,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
