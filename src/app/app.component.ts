import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home-page/home-page';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'LoginPage';

  pages: Array<{icon: string, title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { icon: 'fa fa-home fa-fw  iconPosition', title: 'Home', component: 'HomePage' },
      { icon: 'fa fa-user fa-fw  iconPosition', title: 'Distributor', component: 'DistributorPage'},
      { icon: 'fa fa-archive fa-fw  iconPosition', title: 'Inventory', component: 'InventoryPage'},
      { icon: 'fa fa-search fa-fw  iconPosition', title: 'Search', component: 'SearchPage'},
      { icon: 'fa fa-outdent fa-fw  iconPosition', title: 'Logout', component: 'LoginPage'}

    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    if (page.component === 'LoginPage') {
      this.logout();
    }
    if (page.component === 'HomePage' || page.component === 'LoginPage')
      this.nav.setRoot(page.component);
    else
      this.nav.push(page.component);
  }

  logout() {
    console.log('Inside Logout');
    localStorage.removeItem('distributor');
  }
}
