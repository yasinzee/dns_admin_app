export let cfg = {
    // apiUrl: 'http://dnstest2.ap-south-1.elasticbeanstalk.com/',
    apiUrl: 'http://localhost:8080/',
    // apiUrl: 'http://dnsadmin.ap-south-1.elasticbeanstalk.com/',
    addProduct: 'product/add',
    authenticate: 'login/authenticate',
    saveOrder: 'distributor/order',
    fetchOrders: 'distributor/orders/fetch',
    fetchOrderedProducts: 'distributor/orders/product',
    addConsumer: 'consumer/add',
    fetchConsumerList: 'consumer/fetch',
    fetchConsumerListByCity: 'consumer/city_wise_consumer',
    updateConsumer: 'consumer/update',

    addDistributor: 'distributor/add',
    fetchAllProducts: 'product/all',
    fetchAllCity: 'search/city',
    fetchAllState: 'search/state',
    fetchCityWiseConsumerAndDistributor: 'search/city_wise_consumer',
    inventoryDetails: 'inventory/fetch',
    updateInventory: 'inventory/update',
    fetchAllInventoryTransaction: 'inventory/transactions/all',
    fetchAllDistributor: 'distributor/all',
    updateDistributor: 'distributor/update',
    changeDistributorPassword: 'distributor/change/password',


    productWiseSearch: 'search/product',
    distributorWiseSearch: 'search/distributor',
    consumerWiseSearch: 'search/consumer',
    distributorFilterResultDetail : 'search/distributor/detail'

}

export let finalVar = {
    //Const variables
    distributor: 'DISTRIBUTOR',
    product: 'PRODUCT',
    consumer: 'CONSUMER',
    inventory: 'INVENTORY',
    inventoryAdd: 'ADD',
    inventorySubtract: 'SUB',

}